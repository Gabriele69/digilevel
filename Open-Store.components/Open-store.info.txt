Info:

Author: JassMan23

Packager/Publisher: JassMan23

Source: https://gitlab.com/JassMan23/digilevel

License: GNU GENERAL PUBLIC LICENSE Version 3

Category: Utilities

Updated: June 17th 2019

Published: June 17th 2019

Framework: ubuntu-sdk-16.04

Architecture: all


Description:
DigiLevel. A rewrite of @mhall119 's Level Finder project. Thanks to mhall119 and rschroll for their original ideas. Thanks also to the authors of the  examples in the QMLCreator for Android/iOS by Oleg Yadrov (olegyadrov) and Alfred E. Neumayer (fredldotme).

Current Version: 0.5.7

import QtQuick 2.9
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import QtSensors 5.9

MainView {
    id: root
    objectName: 'digitalLevel'
    applicationName: 'digilevel.jassman23'
    automaticOrientation: false

    property color _backgndColor: null //set to null for now, otherwise you get "property value set multiple times" when restoring from settings
    property color _textColor: null
    property int _textHeight: null
    property color _datumColor: null
    property int _datumWidth: null
    property color _gridColor: null
    property int _gridWidth: null

    property int levelOrientation: 90
    property bool held: false
    property real heldValue: 0
    property bool abslvl: true       // level shows absolute / relative
    property bool inclvl: false      // level shows incline ratio / °
    property bool radialBubble: false
    property bool camLevel: false
    property bool vertDisp: true

    property real theta: 0  // Angle of acceleration vector in X-Y plane
    property real lastTheta: 0
    property real relTheta: 0
    property real phi   // Angle of acceleration out of X-Y plane
    property real grdnt // Gradient in X-Y plane

    property real ax: 0
    property real ay: 0
    property real az: 0


    Accelerometer {
        id: accel
        active: true
        dataRate: 1000
        property real smoothing: 0.01
        property real smoothingT: 0.05

        onReadingChanged: {
            ax = smoothing * reading.x + (1 - smoothing) * ax
            ay = smoothing * reading.y + (1 - smoothing) * ay
            az = smoothing * reading.z + (1 - smoothing) * az
            if (phi >60) {radialBubble = true}
            if (phi <40) {radialBubble = false}

            if (radialBubble){
                phi = Math.atan2(az, Math.sqrt(ax*ax + ay*ay)) * 180 / Math.PI
                if(!held){
                    //theta = smoothingT * (Math.atan2(ay, ax) * 180 / Math.PI) + (1 - smoothingT) * theta
                    theta = (Math.atan2(ay, ax) * 180 / Math.PI)
                } else {
                    lastTheta = theta
                }            
                abslvl = true
            } else
                if(!held){
                    phi = Math.atan2(az, Math.sqrt(ax*ax + ay*ay)) * 180 / Math.PI
                    if (levelOrientation == 0){
                        theta = Math.atan2(ay, ax) * 180 / Math.PI
                        if (ax != 0){grdnt = Math.abs(ay) / Math.abs(ax)}
                    } else {
                        theta = Math.atan2(ax, ay) * 180 / Math.PI
                        if (ay != 0){grdnt = Math.abs(ax) / Math.abs(ay)}
                    }               
            } else {
                lastTheta = theta
            }
        }
    }

    ListModel {
        id: _notesList
    }

    function listModelToString(){
        var datamodel = []
        for (var i = 0; i < _notesList.count; ++i)
            datamodel.push(_notesList.get(i))
        _settings.notesData = JSON.stringify(datamodel)
    }

    function fillListModel(){
        _notesList.clear()
        //console.log('>' + _settings.notesData + '<')

        if(_settings.notesData == "[]") {
            _settings.notesData = i18n.tr('[{"angle":"30.00","dateTime":"Monday @ 11h59:59pm","description":"Example"}]')
        }
        var datamodel = JSON.parse(_settings.notesData)
        for (var i = 0; i < datamodel.length; ++i) _notesList.append(datamodel[i])
    }

    Settings {
        id:_settings
        property color backgroundColor: theme.palette.normal.background
        property color textColor: theme.palette.normal.baseText
        property int textHeight: Label.Large //units.dp(24)
        property color datumColor: theme.palette.normal.negative
        property int datumWidth: units.dp(3)
        property color gridColor: theme.palette.normal.positive
        property int gridWidth: units.dp(1)
        property string notesData: i18n.tr('[{"angle":"30.00","dateTime":"Monday @ 11h59:59","description":"Example"}]')
    }
/*
    width: (parent.width>units.gu(60))? units.gu(45) : parent.width
    height: (parent.height>units.gu(100))? units.gu(75) : parent.height
*/

    PageStack {
        id: mainStack
    }

    Component.onCompleted: {
        console.log("==== App launched " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
        _backgndColor = _settings.backgroundColor
        _textColor = _settings.textColor
        _textHeight = _settings.textHeight
        _datumColor = _settings.datumColor
        _datumWidth = _settings.datumWidth
        _gridColor = _settings.gridColor
        _gridWidth = _settings.gridWidth
        fillListModel()
        mainStack.push(Qt.resolvedUrl("LevelPage.qml"))
    }

    Component.onDestruction: {
        _settings.backgroundColor = _backgndColor
        _settings.textColor = _textColor
        _settings.textHeight = _textHeight
        _settings.datumColor = _datumColor
        _settings.datumWidth = _datumWidth
        _settings.gridColor = _gridColor
        _settings.gridWidth = _gridWidth
        listModelToString()
    }

}

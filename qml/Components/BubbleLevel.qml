import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    anchors.fill: parent
    anchors.centerIn: parent
    color: "transparent"


    function distForAngle(angle) {
        return width / 2 * Math.sin(angle * Math.PI / 90)
    }

    Rectangle {             //the floating bubble
        width: units.gu(3)
        height: width
        radius: width/2
        color: _datumColor

        x: (bubbleLevel.width - width)   / 2 + ax/Math.sqrt(ax*ax+ay*ay) * distForAngle(Math.abs(phi))
        y: (bubbleLevel.height - height) / 2 - ay/Math.sqrt(ax*ax+ay*ay) * distForAngle(Math.abs(phi))
    }

    Repeater {
        //model: [40, 25, 15, 10, 5, 1]
        //model: [45, 28, 21, 15, 10, 6, 2]
        model: [40, 30, 25, 20, 15, 10, 5, 1]
        Rectangle {
            anchors.centerIn: parent
            width: 2 * bubbleLevel.distForAngle(modelData)
            height: width
            radius: width/2
            border.color: _gridColor
            border.width: units.dp(1)
            color: "transparent"

            Label {
                x: 0.85 * parent.width
                y: 0.85 * parent.height
                text: modelData + "&deg;"
                textFormat: Text.RichText
                color: _gridColor
            }
        }
    }
}

